module.exports = {
    plugins: [
        require('glhd-tailwindcss-transitions')({
            transitionDelay: {
                default: '0s'
            }
        }),
    ],
    theme: {
        fontFamily: {
            'body': ['nimbus-sans', 'sans-serif'],
        },
        colors: {
            green: {
                100:'#86AA90',
                200:'#99FFB3',
                400:'#C6FFD5',
            },
            transparent: 'transparent',
            black: '#000',
            white: '#fff',
            grey: {
                light: '#F3F3F3',
                default: '#d8d8d8',
                dark: '#222222'
            },
            error: '#ff3333',
        }
    },
    variants: {
        opacity: ['responsive', 'hover'],
    }
}
